package com.flyfox.util.cache;

import com.flyfox.util.cache.impl.MemorySerializeCache;
import com.flyfox.util.serializable.Serializer;

/**
 * 请使用MemorySerializeCache
 * 
 * 2015年4月26日 下午8:23:59
 * flyfox 330627517@qq.com
 */
@Deprecated
public class MemCache extends MemorySerializeCache implements Cache {

	public MemCache(Serializer serializer) {
		super(serializer);
	}

}
