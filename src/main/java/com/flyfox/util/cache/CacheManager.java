package com.flyfox.util.cache;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.flyfox.util.cache.impl.MemorySerializeCache;
import com.flyfox.util.serializable.SerializerManage;

/**
 * 缓存管理接口
 * 
 * 2015年4月26日 下午8:47:45 flyfox 330627517@qq.com
 */
public class CacheManager {

	private static ConcurrentHashMap<String, Cache> cacheManager = new ConcurrentHashMap<String, Cache>();
	static ICacheManager _CreateCache;

	protected CacheManager() {
	}

	static {
		_CreateCache = new ICacheManager() {
			public Cache getCache() {
				return new MemorySerializeCache(SerializerManage.get("java"));
			}
		};
	}

	public static void setCache(ICacheManager thisCache) {
		_CreateCache = thisCache;
	}

	public static Cache get(String name) {
		Cache cache = cacheManager.get(name);
		if (cache == null) {
			synchronized (cacheManager) {
				cache = cacheManager.get(name);
				if (cache == null) {
					cache = _CreateCache.getCache();
					cache.name(name);
					cacheManager.put(name, cache);
				}
			}
		}
		return cache;
	}

	public static int size() {
		return cacheManager.size();
	}

	public static Collection<Cache> values() {
		return cacheManager.values();
	}

	public static Set<String> keys() {
		return cacheManager.keySet();
	}

}